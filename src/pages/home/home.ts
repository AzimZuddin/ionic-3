import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service/auth-service';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  
  public userDetails : any;
  public responseData: any;
  public dataset : any;

  userPostData = {"user_id":"","token":""};
  app: any;

  constructor(public navCtrl: NavController, public authService:AuthService) {
  const data = JSON.parse(localStorage.getItem('userData'));
  this.userDetails = data.userData;

  this.userPostData.user_id = this.userDetails.user_id;
  this.userPostData.token = this.userDetails.token;

  this.getFeed();

}


getFeed(){
  this.authService.postData(this.userPostData,'feed').then((result) => {
    this.responseData = result;
    if(this.responseData.feedData){
    this.dataset = this.responseData.feedData;
    console.log(this.dataset);
    }
    else{
      console.log("No access");
    }

  }, (err) => {
    // Error log
  });
}

backToWelcome(){
   const root = this.app.getRootNav();
   root.popToRoot();
}

logout(){
     localStorage.clear();
     setTimeout(() => this.backToWelcome(), 1000);
}

}