import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ToyotaPage } from './toyota';

@NgModule({
  declarations: [
    ToyotaPage,
  ],
  imports: [
    IonicPageModule.forChild(ToyotaPage),
  ],
})
export class ToyotaPageModule {}
