import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AudiPage } from './audi';

@NgModule({
  declarations: [
    AudiPage,
  ],
  imports: [
    IonicPageModule.forChild(AudiPage),
  ],
})
export class AudiPageModule {}
