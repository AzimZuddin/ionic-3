import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HondaPage } from './honda';

@NgModule({
  declarations: [
    HondaPage,
  ],
  imports: [
    IonicPageModule.forChild(HondaPage),
  ],
})
export class HondaPageModule {}
