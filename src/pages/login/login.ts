import { Component } from '@angular/core';
import {  NavController, ToastController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';
import { AuthService } from '../../providers/auth-service/auth-service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  responseData : any;
  userData = {"username": "","password": ""};

  constructor(public navCtrl: NavController, public authService:AuthService, private toastController:ToastController ) {
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad Login')
  }

  login(){
     this.authService.postData(this.userData,'login').then((result) => {
      this.responseData = result;
      if(this.responseData.userData){
      console.log(this.responseData);
      if (this.responseData.userData){
        localStorage.setItem('userData', JSON.stringify(this.responseData));
        this.navCtrl.push(TabsPage);
      }

    }
      else{ this.presentToast("User already exists"); }
    }, (err) => {
      // Error log
    });

  }

  presentToast(msg) {
    let toast = this.toastController.create({
      message: msg,
      duration: 2000
    })
  }

  signup(){
    //Signup page link
    this.navCtrl.push(SignupPage);
  }

}