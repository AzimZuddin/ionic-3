import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NissanPage } from './nissan';

@NgModule({
  declarations: [
    NissanPage,
  ],
  imports: [
    IonicPageModule.forChild(NissanPage),
  ],
})
export class NissanPageModule {}
