import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MitsubishiPage } from './mitsubishi';

@NgModule({
  declarations: [
    MitsubishiPage,
  ],
  imports: [
    IonicPageModule.forChild(MitsubishiPage),
  ],
})
export class MitsubishiPageModule {}
